package org.thegeekgroup.n_brst;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;


public class NBRST_Main extends ActionBarActivity {

    private SensorManager mSensorManager;
    private boolean listenEnable = false;
    private SortedSet<Integer> availableSensorTypes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nbrst__main);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        availableSensorTypes = new TreeSet<Integer>();

        List<Sensor> sensorList = mSensorManager.getSensorList(Sensor.TYPE_ALL);

        for(Sensor s : sensorList){
            if (s.getReportingMode() == Sensor.REPORTING_MODE_CONTINUOUS || s.getReportingMode() == Sensor.REPORTING_MODE_ON_CHANGE)
            availableSensorTypes.add(s.getType());
            Log.i("NBRST_APP", s.getName());
        }
        Log.d("NBRST_APP",Arrays.toString(availableSensorTypes.toArray()));
        final TextView textBox = (TextView) findViewById(R.id.LogWindow);
        textBox.setMovementMethod(new ScrollingMovementMethod());
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nbrst__main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void testClick(View v){
        final TextView textBox = (TextView) findViewById(R.id.LogWindow);

        List<Sensor> sensorList = mSensorManager.getSensorList(Sensor.TYPE_ALL);

        for(Sensor s : sensorList){
            Log.i("NBRST_APP",s.getName());
            //textBox.append(s.getName()+"\n");
        }
    }

    public void enableClick(View v){

        final SensorEventListener mSensorListener = new SensorEventListener() {
            @Override
            public void onAccuracyChanged(Sensor arg0, int arg1) {
            }

            @Override
            public void onSensorChanged(SensorEvent event) {
                if (listenEnable) {
                    Sensor sensor = event.sensor;
                    Log.v("NBRST_APP", sensor.getName() + ": " + Arrays.toString(event.values));
                }
            }

        };

        if(!listenEnable) {
            listenEnable = true;
            for(int type : availableSensorTypes)
                mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(type), SensorManager.SENSOR_DELAY_NORMAL);
        } else {
            listenEnable = false;
            mSensorManager.unregisterListener(mSensorListener);
        }
        Log.i("NBRST_APP",listenEnable?"Listening Enabled":"Listening Disabled");
    }

    public Map<Integer,float[]> getSensorSnapshot(){
        final Map<Integer,float[]> sensorMeasures = new HashMap<Integer, float[]>();
        List<Integer> availableSensors = new ArrayList<Integer>();

        SensorEventListener mSensorListener = new SensorEventListener() {
            @Override
            public void onAccuracyChanged(Sensor arg0, int arg1) {
            }

            @Override
            public void onSensorChanged(SensorEvent event) {
                Sensor sensor = event.sensor;
                if(sensorMeasures.containsKey(sensor.getType())) {
                    Log.v("NBRST_APP", sensor.getType() + ": " + Arrays.toString(event.values));
                    sensorMeasures.put(sensor.getType(), event.values);
                }
            }
        };

        for(int type : availableSensorTypes)
            mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(type), SensorManager.SENSOR_DELAY_NORMAL);


        return sensorMeasures;
    }
}
